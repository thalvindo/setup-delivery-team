SSM Warehouse sedang setup tim delivery untuk mengirim order ke pelanggan mereka. Ada 2 team leader dan N anggota delivery. Sekarang team leader ingin membagi N anggota delivery tadi menjadi 2 team. Kita beri nomor untuk masing2 anggota delivery dari nomer 1 sampai N.
Cara pembagian timnya berlangsung sebagai berikut:
tim leader 1 akan memilih satu anggota untuk tim dia dari anggota team delivery yang ada
tim leader 2 akan memilih satu anggota dari anggota delivery yang tersisa
Kembali ke nomer 1 sampai tidak ada anggota tim delivery tersisa yang belum masuk tim.
Tetapi .. ketika memilih anggota, sebenarnya tiap tim leader sudah punya daftar urutan prioritas nomer anggota yang ingin dia ambil masuk sebagai timnya (ada N nomor). Tim leader akan memilih nomer anggota yang ingin diajak bergabung ke timnya berdasar listnya dan jika nomer tersebut belum bergabung dengan tim lain.
Contoh: pref1 adalah array yang berisi nomor2 anggota team yang diinginkan oleh team leader 1 berdasar prioritas. pref1[0] adalah nomer anggota tim delivery yang jadi prioritas pertama dari tim leader 1, pref1[1] adalah nomer anggota yang jadi prioritas kedua dan seterusnya.
pref2 sejenis dengan pref1, tetapi merupakan daftar prioritas dari team leader 2. Jika misal tim leader 1 giliran memilih anggota team sedang nomer angota yang jadi prioritasnya sudah dipilih oleh team leader 2, maka team leader 1 akan memilih anggota team dengan index prioritas selanjutnya yang belum bergabung ke tim lain.
Tugas:
Print string yang berisi karakter sebanyak N. Karakter nomer i (i dimulai dari 0) dalam string tersebut harus bernilai '1' jika anggota delivery dengan nomer i+1 adalah anggota dari team leader 1, and bernilai '2' jika menjadi anggota team leader 2.
Contoh:
4
1 2 3 4
1 2 3 4
Returns: "1212"
Penjelasan: Ada 4 anggota team delivery yang akan dibagi menjadi 2 team. Tiap team leader punya daftar prioritas yang sama: anggota nomer 1 jadi prioritas 1, anggota nomer 2 jadi prioritas 2, anggota nomer 3 jadi prioritas 3, anggota nomer 4 jadi prioritas terakhir. Jadi, team leader 1 akan memilih anggota delivery nomer 1, team leader 2 akan memilih anggota delivery nomer 2 karena nomer 1 udah jadi member team leader 1. Kemudian team leader 1 akan memilih anggota delivery nomer 3 karena nomer 2 udah jadi anggota team leader 2. dan team leader 2 akan memilih anggota delivery nomer 4. Sehingga team leader 1 akan punya anggota 1 dan 3, sedang team leader 2 akan punya anggota 2 dan 4 -> "1212"
Input Format
Baris pertama berisi N, angka jumlah anggota delivery
Baris kedua berisi daftar prioritas dari team leader 1 dipisahkan oleh space (pref1)
Baris ketiga berisi daftar prioritas dari team leader 2 dipisahkan oleh space (pref2)
Constraints
N antara 2 dan 50, termasuk 50 dan 2.
pref1 berisi sejumlah N angka
Angka2 di pref1 berisi nomer dari 1 sampai N dan masing2 angka hanya muncul sekali.
pref2 berisi sejumlah N angka
Angka2 di pref2 berisi nomer dari 1 sampai N dan masing2 angka hanya muncul sekali.
Output Format
Print string yang berisi karakter sebanyak N. Karakter nomer i (i dimulai dari 0) dalam string tersebut harus bernilai '1' jika anggota delivery dengan nomer i+1 adalah anggota dari team leader 1, and bernilai '2' jika menjadi anggota team leader 2.
Sample Input 0
4
1 2 3 4
1 2 3 4
Sample Output 0
1212
Explanation 0
Ada 4 anggota team delivery yang akan dibagi menjadi 2 team. Tiap team leader punya daftar prioritas yang sama: anggota nomer 1 jadi prioritas 1, anggota nomer 2 jadi prioritas 2, anggota nomer 3 jadi prioritas 3, anggota nomer 4 jadi prioritas terakhir. Jadi, team leader 1 akan memilih anggota delivery nomer 1, team leader 2 akan memilih anggota delivery nomer 2 karena nomer 1 udah jadi member team leader 1. Kemudian team leader 1 akan memilih anggota delivery nomer 3 karena nomer 2 udah jadi anggota team leader 2. dan team leader 2 akan memilih anggota delivery nomer 4. Sehingga team leader 1 akan punya anggota 1 dan 3, sedang team leader 2 akan punya anggota 2 dan 4 -> "1212"
Sample Input 1
3
3 2 1
1 3 2
Sample Output 1
211
Explanation 1
Team leader 1 akan ambil anggota delivery nomer 3, dan team leader 2 akan ambil nomer 1. Selanjutnya team leader 1 akan ambil nomer 2. Catatan: ketika jumlah teamnya ganjil, maka team leader 1 akan punya anggota lebih banyak satu dibanding team leader 2.