
const processData = (input) => {
    //Enter your code here
    // let length = parseInt(input.slice(0,1));
    // //https://stackoverflow.com/questions/5963182/how-to-remove-spaces-from-a-string-using-javascript
    // let rawInput = input.replace(/\s/g, '');
    

    // let rawInput = input.replaceAll("\n", ' ');
    // rawInput = rawInput.split(' ');

    let rawInput = input.split(' ');
    let length = parseInt(rawInput[0]);

    let firstLeaderPriority = Array();
    let secondLeaderPriority = Array();
    let storedMember = new Map();
    
    let firstLeaderTurn = true;
    let index = 0;
    let answer = '';
    let j = 1;
    
    // break down the raw data to smaller chunks
    for(let i=1; i<length + 1; i++) {
        firstLeaderPriority.push(rawInput[i]);
    };
    
    // break down the raw data to smaller chunks
    for(let i=length + 1; i<parseInt(rawInput.length); i++) {
        secondLeaderPriority.push(rawInput[i]);
    };

    // console.log(firstLeaderPriority);
    // console.log(secondLeaderPriority);
    
    // check each index for each N numbers of delivery member
    while(index < length) {
        if(firstLeaderTurn) {
            // if first leader priority, first leader gets to choose first.
            if(storedMember.get(firstLeaderPriority[index]) === undefined) {
                storedMember.set(firstLeaderPriority[index], 1);
            } else {
                let k = index;
                while(k < firstLeaderPriority.length - 1) {
                    if(storedMember.get(firstLeaderPriority[k+1]) === undefined) {
                        storedMember.set(firstLeaderPriority[k+1], 1);
                        break;
                    }
                    k++;
                }
            }
            if(storedMember.get(secondLeaderPriority[index]) === undefined) {
                storedMember.set(secondLeaderPriority[index], 2);
            }
        }
        
        if(!firstLeaderTurn) {
            // if it's not the first leader's turn. then the second leader gets the initiative.
            if(storedMember.get(secondLeaderPriority[index]) === undefined) {
                storedMember.set(secondLeaderPriority[index], 2);
            }
            if(storedMember.get(firstLeaderPriority[index]) === undefined) {
                storedMember.set(firstLeaderPriority[index], 1);
            }
        }
        
        // update the turn and index.
        firstLeaderTurn = !firstLeaderTurn;
        index++;
    }
    // sort the map by key from the first to last delivery member.
    // https://bobbyhadz.com/blog/javascript-sort-keys-in-map#:~:text=Use%20the%20sort()%20method,sort%20using%20the%20sort%20method.
    storedMember = new Map([...storedMember].sort());

    // get the value for each map data that is sorted.
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map/values
    const iterator = storedMember.values();
    while(j <= storedMember.size) {
        answer += iterator.next().value;
        j++;
    }
    return answer;
} 

// let input = '4 1 2 3 4 1 2 3 4';
let input = '20 13 14 15 16 17 18 19 20 12 1 2 3 4 5 6 7 8 9 10 11 10 9 8 7 11 13 14 15 16 17 18 19 20 6 12 5 4 3 2 1';
// let input = '5 3 2 4 5 1 4 1 3 5 2';
// let input = '5 1 2 3 4 5 5 3 4 2 1';
// let input = '5 1 2 3 4 5 5 4 3 2 1';
//another test data
// '3 3 2 1 1 3 2'


//5 1 2 3 4 5 5 3 4 2 1
const ans = processData(input);

console.log(ans);
